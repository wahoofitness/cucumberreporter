import com.slack.api.methods.SlackApiException;
import org.apache.log4j.BasicConfigurator;

import java.io.IOException;

public class MainMethod {

    public static void main(String[] args){
        BasicConfigurator.configure();

        Reporter reporter = new Reporter();
        reporter.cucumberParser("/Users/bryanoreilly/Desktop/cucumber2.json");
        System.out.println(reporter.getSummary());
        System.out.println(reporter.getFailedTests());

        System.out.println("Total: "+ reporter.getTotalString());
        System.out.println("Passed: "+ reporter.getPassedString());
        System.out.println("Failed: "+ reporter.getFailedString());

        CallSlack callSlack = new CallSlack("sufferfest");
        String slack = callSlack.prepareSlackMessage(reporter);
        System.out.println(slack);

//        try {
//            callSlack.postToAutomationResultsAsSuffer(slack);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SlackApiException e) {
//            e.printStackTrace();
//        }
        //callSlack cs = new callSlack("sufferfest");

    }
}
