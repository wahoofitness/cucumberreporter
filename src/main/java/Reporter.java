import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Reporter {

    private final HashMap<String, Integer> summary;
    private final List<String> failed;

    public Reporter(){
        this.summary = new HashMap<String, Integer>(){};
        this.failed = new ArrayList<String>(){};
    }

    public HashMap<String, Integer> getSummary() {
        return summary;
    }

    public List<String> getFailedTests() {
        return failed;
    }

    public int getTotal() { return getSummary().get("total"); }
    public int getPassed() { return getSummary().get("passed"); }
    public int getFailed() { return getSummary().get("failed"); }

    public String getTotalString() { return Integer.toString(this.summary.get("total")); }
    public String getPassedString() { return Integer.toString(this.summary.get("passed")); }
    public String getFailedString() { return Integer.toString(this.summary.get("failed")); }


    public void cucumberParser(String path) {
        JSONParser parser = new JSONParser();
        this.summary.put("total", 0);
        this.summary.put("passed", 0);
        this.summary.put("failed", 0);


        int i = 0;

        try {
            JSONArray a = (JSONArray) parser.parse(new FileReader(path));
            for (Object feature : a)
            {
                JSONObject featureJson = (JSONObject) feature;
                for (Object test : (JSONArray) featureJson.get("elements")){
                    JSONObject  testJson = (JSONObject) test;
                    for (Object step : (JSONArray) testJson.get("steps")){
                        JSONObject  stepJson = (JSONObject) step;
                        JSONObject  resultJson = (JSONObject) stepJson.get("result");
                        String status = (String) resultJson.get("status");
                        if (status.equals("failed")){
                            summary.put("failed", summary.get("failed") + 1);
                            failed.add((String) testJson.get("name"));
                            break;
                        }
                        else if(status.equals("xfailed")){
                            summary.put("xfailed", summary.get("xfailed") + 1);
                            failed.add((String) testJson.get("name"));
                            break;
                        }
                    }
                    summary.put("total", summary.get("total") + 1);
                }
            }
            summary.put("passed", (summary.get("total") - summary.get("failed")));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

//    public int[] cucumberParserValues(String path) {
//        JSONParser parser = new JSONParser();
//        HashMap<String, Integer> summary = new HashMap<String, Integer>(){};
//        summary.put("total", 0);
//        summary.put("passed", 0);
//        summary.put("failed", 0);
//
//        try {
//            JSONArray a = (JSONArray) parser.parse(new FileReader("src/main/java/files/cucumber2.json"));
//            for (Object feature : a)
//            {
//                JSONObject featureJson = (JSONObject) feature;
//                for (Object test : (JSONArray) featureJson.get("elements")){
//                    JSONObject  testJson = (JSONObject) test;
//                    for (Object step : (JSONArray) testJson.get("steps")){
//                        JSONObject  stepJson = (JSONObject) step;
//                        JSONObject  resultJson = (JSONObject) stepJson.get("result");
//                        String status = (String) resultJson.get("status");
//                        if (status.equals("failed")){
//                            summary.put("failed", summary.get("failed") + 1);
//                            failed.add((String) testJson.get("name"));
//                            break;
//                        }
//                        else if(status.equals("xfailed")){
//                            summary.put("xfailed", summary.get("xfailed") + 1);
//                            failed.add((String) testJson.get("name"));
//                            break;
//                        }
//                    }
//                    summary.put("total", summary.get("total") + 1);
//                }
//            }
//            summary.put("passed", (summary.get("total") - summary.get("failed")));
//        } catch (IOException | ParseException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println(summary.toString());
//        System.out.println(summary.get("passed").toString());
//        System.out.println(summary.get("failed").toString());
//        System.out.println(summary.get("total").toString());
//
//        int[] output = new int[2];
//        output[0] = summary.get("total");
//        output[1] = summary.get("failed");
//
//
//        return output;
//    }
}
