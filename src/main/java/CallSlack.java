import com.slack.api.Slack;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import com.slack.api.model.Message;

import java.io.IOException;
import java.text.DecimalFormat;


public class CallSlack {
    private static String botToken;
    private int total_num;
    private int failed_num;
    private int passed_num;
    private String status_str;
    private float passRate_num;
//    private String slackMessage_Template;
//    private String slackMessage;

    private String totalString;
    private String failedString;
    private String passedString;
    private String passRateString;

    public CallSlack(String bot){
        String token = "";
        if (bot.equals("sufferfest"))
            token = "xoxb-3842229679-1181881934432-mniA5xCgIhcPK3f6iXcoEfyV";
        else
            token = "xoxb-3842229679-1181881934432-mniA5xCgIhcPK3f6iXcoEfyV";
        botToken = token;
    }

    public String prepareSlackMessage(Reporter reporter){
        total_num = reporter.getTotal();
        failed_num = reporter.getFailed();
        //passed_num = total_num - failed_num;
        passed_num = reporter.getPassed();
        System.out.println(total_num);
        System.out.println(passed_num);

        float total_float = total_num;
        float passed_float = passed_num;

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        passRate_num = passed_float/total_float;
        System.out.println(passRate_num);

        totalString = reporter.getTotalString();
        failedString = reporter.getFailedString();
        passedString = reporter.getPassedString();
        passRateString = Float.toString(passRate_num*100);

        if (passRate_num > .99){
            status_str = ":white_check_mark:";
        } else if (passRate_num > .50){
            status_str = ":warning:";
        }else {
            status_str = ":small_red_triangle_down:";
        }

//        slackMessage_Template = String.format("*Test Status*:  <http://wf01jenkins02.wahoofitness.com:8080/| Jenkins Report> \n   Status: ",status_str,"    Pass Rate: ",passRateString,"%     # Tests: ",totalString,"   \n      ",passedString," _*Passed*_ ",failedString," _*Failed*_");
        String slackMessage_Template = "*Test Status*:  <http://wf01jenkins02.wahoofitness.com:8080/| Jenkins Report> \n   Status: "+status_str+"    Pass Rate: "+passRateString+"%     # Tests: "+totalString+"   \n      "+passedString+" _*Passed*_ "+failedString+" _*Failed*_";

        return slackMessage_Template;

    }

    public static void postToAutomationResultsAsSuffer(String message) throws IOException, SlackApiException {

        //reporter.cucumberParser("src/main/java/files/cucumber2.json");

        //String slackMessage_Template = String.format("*Test Status*:  <http://wf01jenkins02.wahoofitness.com:8080/| Jenkins Report> \n   Status: ",status_str,"    Pass Rate: ",passRate,"%     # Tests: ",total,"   \n      ",passed," _*Passed*_ ",failed," _*Failed*_");

        Slack slack = Slack.getInstance();
        // Load an env variable
        // If the token is a bot token, it starts with `xoxb-` while if it's a user token, it starts with `xoxp-`
        //String token = System.getenv(botToken);

        // Initialize an API Methods client with the given token
        //MethodsClient methods = slack.methods(token);

        try {
            ChatPostMessageResponse response = slack.methods(botToken).chatPostMessage(req -> req
                    .channel("#delivery-automation-results")
                    .text(message));
            if (response.isOk()) {
                Message postedMessage = response.getMessage();
            } else {
                String errorCode = response.getError(); // e.g., "invalid_auth", "channel_not_found"
            }
        } catch (SlackApiException requestFailure) {
            // Slack API responded with unsuccessful status code (= not 20x)
        } catch (IOException connectivityIssue) {
            // Throwing this exception indicates your app or Slack servers had a connectivity issue.
        }

    }
}
